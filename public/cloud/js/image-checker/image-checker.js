$(function(){
	var $imgPlaceholder = $('script[src$="image-checker.js"]').data('placeholder');
	
	if(!!$imgPlaceholder){
		$("img").each(function(){
			var $this = $(this);
			var $img = $this.data('src');
			
			if(!!$img){
				$this.on('error', function(){
					$this.attr('src', $imgPlaceholder);
				});
				
				$this.attr('src', $img);
			}
		});
	}
});