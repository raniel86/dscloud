class QuillManager {
	
	constructor(){
		this._checkCss()._main();
		
		if(this.isDebug){
			this._debugger();
		}
	}
	
	/**
	 * Check if the quill.snow.css is loaded
	 * @returns {QuillManager}
	 * @private
	 */
	_checkCss(){
		let found = false;
		
		$('link').each(function(){
			if($(this).attr('href').indexOf('quill.snow.css') > -1){
				found = true;
			}
		});
		
		if(!found){
			throw QuillManager.getRowLog('quill.snow.css not found');
		}
		
		return this;
	}
	
	/**
	 * Main function
	 * @private
	 */
	_main(){
		let heightFixed = this.isHeightFixed;
		
		$(QuillManager.selector).each(function(){
			let $this = $(this);
			let v     = $this.val();
			let k     = $this.attr('id');
			let i     = k + '_quillEditor';
			let h     = $this.height() * 2;
			
			if(heightFixed){
				$this.after('<div id="' + i + '" style="height: ' + h + 'px;">' + v + '</div>');
			}
			else{
				$this.after('<div id="' + i + '" style="min-height: ' + h + 'px;">' + v + '</div>');
			}
			
			$this.hide();
			
			try{
				let quill = new Quill('#' + i, {
					theme   : 'snow',
					modules : {
						toolbar : QuillManager.toolbarOptions
					}
				});
				
				quill.on('text-change', function(delta, oldDelta, source){
					$this.val(quill.root.innerHTML);
				});
			}
			catch(e){
				$('#' + i).remove();
				$this.show();
				
				console.error(QuillManager.getRowLog('quill.js not found'));
				console.error(e);
			}
		});
	}
	
	/**
	 * Perform debugger functions
	 * @private
	 */
	_debugger(){
		console.log(QuillManager.getRowLog('debug on'));
		console.log(QuillManager.getRowLog("use the '" + QuillManager.selector + "' selector to manage your textarea"));
		
		// creation of the checkboxes to show the textareas
		$(QuillManager.selector).each(function(){
			let $this = $(this);
			let k     = $this.attr('id');
			let i     = k + '_quillEditor';
			
			$this.before('<div class="form-group form-check"><input type="checkbox" data-target="' + k + '" class="form-check-input quill-show-textarea" id="' + i + '_check"><label class="form-check-label" for="' + i + '_check">Show textarea (debug)</label></div>');
		});
		
		// checkboxes manager
		$('body').on('click', '.quill-show-textarea', function(){
			let $this = $(this);
			
			$('#' + $this.data('target')).toggle($this.is(':checked'));
		});
	}
	
	/**
	 * Create a row ready for log
	 * @param string
	 * @returns {string}
	 */
	static getRowLog(string){
		if(typeof string === 'undefined'){
			throw QuillManager.getRowLog('getRowLog string param not defined');
		}
		
		return 'Quill manager: ' + string;
	}
	
	/**
	 * Return the toolbar options
	 * @returns {*[]}
	 */
	static get toolbarOptions(){
		return [
			[{"header" : ["1", "2", "3", false]}],
			["bold", "italic", "underline", "link"],
			[{"color" : []}, {"background" : []}],
			[{"list" : "ordered"}, {"list" : "bullet"}],
			["clean"]
		];
	}
	
	/**
	 * @returns {string}
	 */
	static get selector(){
		return '.quillValue';
	}
	
	/**
	 * @returns {boolean}
	 */
	get isDebug(){
		return this._getQuillManagerParam('debug') || false;
	}
	
	/**
	 * @returns {boolean}
	 */
	get isHeightFixed(){
		return this._getQuillManagerParam('fix-height') || false;
	}
	
	/**
	 * Return a data param specified in quillManager.js declaration
	 * @param param
	 * @returns {null|*}
	 */
	_getQuillManagerParam(param){
		let ret = null;
		
		$('script').each(function(){
			let $this = $(this);
			let src   = $this.attr('src');
			
			if(!src){
				return;
			}
			
			if(src.indexOf('quillManager.js') > -1){
				ret = $this.data(param);
			}
		});
		
		ret = typeof ret !== 'undefined' ? ret : null;
		
		return ret;
	}
}

$(function(){
	new QuillManager();
});