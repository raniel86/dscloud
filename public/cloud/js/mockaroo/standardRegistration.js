function standardRegistration(callback, asyncronous) {
    if (!callback) {
        console.error('No callback provided for standardRegistration function.');
        return;
    }

    asyncronous = typeof asyncronous === 'undefined' ? true : !!asyncronous;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            callback(JSON.parse(this.responseText));
        }
    };
    xhttp.open('GET', 'https://my.api.mockaroo.com/standard-registration.json?key=93dc4d10', asyncronous);
    xhttp.send();
}