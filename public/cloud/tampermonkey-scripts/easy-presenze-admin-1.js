(function() {
    'use strict';

    var version = '1.2';
    var paramAppended = '&easy-presenze-auto=1';

    $('.welcome').append('<span style="color:#FFF;background-color:#0B0;"><b>Improved by Easy presenze ADMIN ' + version + ' (with love from Raniel)</b></span>');

    setTimeout(function(){
        $('#ricevuti_wrapper').prepend('<button id="btnApprovaTutto" style="width:100%"><b>APPROVA TUTTO</b></button><br><br>');
    }, 1000);

    $('body').on('click', '#btnApprovaTutto', function(e){
        e.preventDefault();

        if(confirm('Continuare con auto approvazione?')){
            var elements = $('.iframe_ricevuti.cboxElement');

            function triggerElement(elements, i){
                var linkIframe = $(elements[i]);
                var newWindow = window.open(linkIframe.attr('href') + paramAppended);

                var intervalChecker = setInterval(function(){
                    if(newWindow.location.href.indexOf(paramAppended) < 0){
                        linkIframe.closest('tr').remove();
                        newWindow.close();
                        clearInterval(intervalChecker);

                        if(elements[i + 1]){
                            triggerElement(elements, i + 1);
                        }
                        else{
                            window.location.reload(true);
                        }
                    }
                }, 500);
            }

            if(elements[0]){
                triggerElement(elements, 0);
            }
        }

        return false;
    });
})();