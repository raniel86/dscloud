(function() {
    'use strict';

    // QUI SI POSSO PERSONALIZZARE ALCUNE VARIABILI
    var motivazioneOmessaTimbratura = 'Dimenticato';
    var buttonsStyle = 'padding:2px;border:1px solid rgb(0, 67, 140);margin-left:2px;text-decoration:none;color:rgb(0, 67, 140);background-color: #EEE;';
    var widthColOperazioni = '28%';
    var version = '3.2';
    // --------------------------------------------

    // <3
    $('.welcome').append('<span style="color:#FFF;background-color:#F00;"><b>Improved by Easy presenze ' + version + ' (with love from Raniel)</b></span>');

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === "undefined" ? true : sParameterName[1];
            }
        }

        return '';
    };

    function getRndInteger(min, max) {
        var str = "" + (Math.floor(Math.random() * (max - min + 1) ) + min);
        var pad = "00";
        var ans = pad.substring(0, pad.length - str.length) + str;
        return ans;
    }

    if(location.href.indexOf('main.asp') > -1){
        // HOMEPAGE
        $('table.cartellino thead tr').append('<th width="' + widthColOperazioni + '">Operazioni</th>');

        function getHrefFunction(txt){
            return $("a.docmodel:contains('" + txt + "')").html('<span style="color:#666;">' + txt + ' (obsoleta)</span>').attr('href');
        }

        var hrefStrao = getHrefFunction('Aut. lavoro straordinario');
        var hrefOmessa = getHrefFunction('Omessa timbratura');
        var hrefCont = getHrefFunction('Continuato');
        var hrefPermesso = getHrefFunction('Permesso Retribuito');
        var hrefServExt = getHrefFunction('Servizio esterno');
        //var hrefMedico = getHrefFunction('Visita medica');

        $('table.cartellino tbody tr').each(function(){
            var tdData = $(this).children('td').eq(1);
            var classes = tdData.attr('class');
            var d = tdData.text();

            var links = [
                '<a style="' + buttonsStyle + '" href="' + hrefStrao + '&dataInizio=' + d + '">Strao</a>',
                '<a style="' + buttonsStyle + '" href="' + hrefOmessa + '&cDataInizio=' + d + '">Omessa</a>',
                '<a style="' + buttonsStyle + '" href="' + hrefCont + '&dataInizio=' + d + '">Continuato</a>',
                '<a style="' + buttonsStyle + '" href="' + hrefPermesso + '&dataInizio=' + d + '">Permesso</a>',
                '<a style="' + buttonsStyle + '" href="' + hrefServExt + '&dataInizio=' + d + '">Servizio esterno</a>'
            ];

            $(this).append('<td class="' + classes + '">' + links.join(' ') + '</td>');
        });

        $('select[name="cPeriod"]').change(function(){
            $(this).next('button').trigger('click');
        });
    }
    else if(location.href.indexOf('omessa.asp') > -1){
        // FORM DI INSERIMENTO OMESSA TIMBRATURA
        var cDataInizio = getUrlParameter('cDataInizio');

        if(cDataInizio !== ''){
            cDataInizio = cDataInizio.split('/');
            cDataInizio = new Date(cDataInizio[1] + '/' + cDataInizio[0] + '/' + cDataInizio[2]);

            var findDay = function(){
                var btnDay = $('td[data-handler="selectDay"][data-month="' + cDataInizio.getMonth() + '"][data-year="' + cDataInizio.getFullYear() + '"] a:contains("' + cDataInizio.getDate() + '")').filter(function() {
                    return $(this).text() == cDataInizio.getDate();
                });

                if(btnDay.length > 0){
                    btnDay.trigger('click');
                }
                else{
                    $('span:contains("<Prec")').trigger('click');
                    setTimeout(findDay, 100);
                }
            };

            $('.ui-datepicker-trigger').trigger('click');
            setTimeout(findDay, 100);
        }

        setInterval(function(){
            if($('.btnEntrata').length > 0){
                return;
            }

            var orariEntrata = ['13:' + getRndInteger(47, 59), '08:' + getRndInteger(53, 59), '08:' + getRndInteger(26, 29)];
            var orariUscita = ['18:' + getRndInteger(1, 11), '17:' + getRndInteger(31, 38), '13:' + getRndInteger(1, 4), '12:' + getRndInteger(31, 34)];

            for(var i = 1; i <= 4; i++){
                $('#verso' + i)
                    .after('<a data-i="' + i + '" data-val="U" href="#" class="btnUscita" style="' + buttonsStyle + '">Uscita</a>')
                    .after('<a data-i="' + i + '" data-val="E" href="#" class="btnEntrata" style="' + buttonsStyle + '">Entrata</a>')
                ;

                $.each(orariEntrata, function(k, v){
                    $('#nMinTimb' + i)
                        .after('<a data-i="' + i + '" data-val="' + v + '" href="#" class="btnOrario btnOrario-' + i + ' btnOrario-' + i + '-E" style="' + buttonsStyle + 'display:none;">' + v + '</a>')
                    ;
                });

                $.each(orariUscita, function(k, v){
                    $('#nMinTimb' + i)
                        .after('<a data-i="' + i + '" data-val="' + v + '" href="#" class="btnOrario btnOrario-' + i + ' btnOrario-' + i + '-U" style="' + buttonsStyle + 'display:none;">' + v + '</a>')
                    ;
                });
            }
        }, 200);

        $('body').on('click', '.btnEntrata, .btnUscita', function(e){
            var v = $(this).data('val');
            var i = $(this).data('i');

            $('#verso' + i).val(v);

            $('.btnOrario-' + i).hide();
            $('.btnOrario-' + i + '-' + v).show();

            e.preventDefault();
            return false;
        });

        $('body').on('click', '.btnOrario', function(e){
            var v = $(this).data('val');
            var i = $(this).data('i');

            v = v.split(':');

            $('#nOraTimb' + i).val(parseInt(v[0]));
            $('#nMinTimb' + i).val(parseInt(v[1]));

            e.preventDefault();
            return false;
        });

        $('#L_MOTIVAZIONE').val(motivazioneOmessaTimbratura);
    }
    else if(location.href.indexOf('documenti.asp') > -1){
        // PAGINE DEGLI ALTRI FORM
        var nMdId = parseInt(getUrlParameter('nMdId'));
        var orari = [];
        var periodi = {};

        $('#dataInizio').val(getUrlParameter('dataInizio'));

        if(nMdId === 2){
            // FORM INSERIMENTO STRAORDINARIO
            orari = ['20:00', '19:30', '19:00', '18:30'];

            $('#oreIni').val('18');
            $('#minIni').val('00');

            $('#note').focus();
        }
        else if(nMdId === 12){
            // FORM RICHIESTA CONTINUATO
            orari = ['14:00', '13:30'];

            $('#oreIni').val('13');
            $('#minIni').val('00');
        }
        else if(nMdId === 4){
            // FORM RICHIESTA PERMESSO
            periodi = {
                'Pomeriggio': {'init': '14:00', 'end': '18:00'},
                'Mattino': {'init': '08:30', 'end': '12:30'}
            };
        }
        else if(nMdId === 3){
            // FORM SERVIZIO ESTERNO
            $('#oreIni').val('08');
            $('#minIni').val('30');

            $('#oreEnd').val('18');
            $('#minEnd').val('00');
        }

        // default note
        $('#note').val(window.localStorage.getItem("last_node_" + nMdId)).focus();
        $('#richiedi').click(function(e){
            e.preventDefault();

            window.localStorage.setItem("last_node_" + nMdId, $('#note').val());

            return true;
        });

        $.each(orari, function(k, v){
            $('#minEnd').after('<a href="#" data-end="' + v + '" class="periodSetter" style="' + buttonsStyle + '">' + v + '</a>');
        });

        $.each(periodi, function(k, v){
            $('#minIni').after('<a href="#" data-init="' + v.init + '" data-end="' + v.end + '" class="periodSetter" style="' + buttonsStyle + '">' + k + '</a>');
        });

        $('body').on('click', '.periodSetter', function(e){
            try{
                var init = $(this).data('init').split(':');

                $('#oreIni').val(init[0]);
                $('#minIni').val(init[1]);
            }
            catch(e){}

            try{
                var end = $(this).data('end').split(':');

                $('#oreEnd').val(end[0]);
                $('#minEnd').val(end[1]);
            }
            catch(e){}

            $('#note').focus();

            e.preventDefault();
            return false;
        });
    }

    $('table.cartellino td:contains(Documento in attesa di approvazione)').text('Attesa');

})();