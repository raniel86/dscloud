<?php

function assetsMap($source_dir, $directory_depth = 0, $hidden = false){
	if($fp = @opendir($source_dir)){
		$filedata = [];
		$new_depth = $directory_depth - 1;
		$source_dir = rtrim($source_dir, '/') . '/';
		
		while(false !== ($file = readdir($fp))){
			// Remove '.', '..', and hidden files [optional]
			if(!trim($file, '.') OR ($hidden == false && $file[0] == '.')){
				continue;
			}
			
			if(($directory_depth < 1 OR $new_depth > 0) && @is_dir($source_dir . $file)){
				$filedata[$file] = assetsMap($source_dir . $file . '/', $new_depth, $hidden);
			}
			else{
				$filedata[] = $file;
			}
		}
		
		closedir($fp);
		
		return $filedata;
	}
	echo 'can not open dir';
	
	return false;
}

$files1 = assetsMap('./cloud/js');
foreach($files1 as $k => $item){
	$files1['js/' . $k] = $item;
	unset($files1[$k]);
}

$files2 = assetsMap('./cloud/tampermonkey-scripts');
$files2 = ['tampermonkey-scripts' => $files2];

$files = $files1 + $files2;
ksort($files);

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
echo json_encode($files, true);